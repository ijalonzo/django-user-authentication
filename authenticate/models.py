from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Account(models.Model):
    date_joined = models.DateTimeField(auto_now=True)
    user = models.OneToOneField(User)
    last_name = models.CharField(max_length=20)
    first_name = models.CharField(max_length=20)
    photo = models.FileField(
        verbose_name='photo',
        upload_to='photos/',
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.first_name + " " + self.last_name
