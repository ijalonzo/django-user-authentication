from django.shortcuts import render
from .models import *

# Create your views here.
def dashboard(request):
    account = Account.objects.filter(user=request.user)[0]
    context = {'account': account}
    return render(request, 'dashboard.html', context)
